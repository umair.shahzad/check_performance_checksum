include(ExternalProject)

if(NOT TARGET crc32_repo)
ExternalProject_Add(crc32_repo
    GIT_REPOSITORY https://github.com/stbrumme/crc32.git
    PREFIX ${CMAKE_CURRENT_BINARY_DIR}/crc32
    CONFIGURE_COMMAND ""
    BUILD_COMMAND ""
    INSTALL_COMMAND ""
)
endif()

ExternalProject_Get_Property(crc32_repo SOURCE_DIR)
set(CRC32_INCLUDE_DIR ${SOURCE_DIR})

if(NOT TARGET crc32_project)
ExternalProject_Add(crc32_project
    DOWNLOAD_COMMAND ""
    CONFIGURE_COMMAND ""
    BUILD_COMMAND gcc -c -o crc32.o ${CRC32_INCLUDE_DIR}/Crc32.cpp &&
                  ar rcs libcrc32.a crc32.o
    INSTALL_COMMAND ""
)
endif()

ExternalProject_Get_Property(crc32_project BINARY_DIR)
set(CRC32_LIB_DIR ${BINARY_DIR})


add_dependencies(crc32_project crc32_repo)